export const state = {
  availability: []
};

export const getters = {
  availability: (state) => {
    return state.availability
  }
};

export const actions = {
  async fetchAvailability({commit},manufacturer) {
    try {
      const availability = await this.$axios.$get('api/v2/availability/'+manufacturer)
      commit('setAvailability', availability);
    }catch (e) {
      commit('setAvailability', []);
    }

  },

}

export const mutations = {
  setAvailability: (state, availability) => (state.availability = availability),
};


export default {
  state,
  getters,
  actions,
  mutations
};
