export const state = {
  products: []
};

export const getters = {
  allProducts: (state) => {
    return state.products
  }
};

export const actions = {
  async fetchProducts({commit},category) {
    try {
      const products = await this.$axios.$get('api/v2/products/' + category)
      commit('setProducts', products);
    } catch (e) {
      commit('setProducts', []);
    }
  },

}

export const mutations = {
  setProducts: (state, products) => (state.products = products),
};


export default {
  state,
  getters,
  actions,
  mutations
};
